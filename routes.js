const express = require("express")
const Post = require("./models/Post") 
const Blog = require("./models/Blog") 
const router = express.Router()
var path = require('path');




// Router Untuk SPA page ====
router.get("/", async (req, res) => {

		res.sendFile(path.join(__dirname + '/view/index.html'));
	
	
	})

	router.get("/admin", async (req, res) => {

		res.sendFile(path.join(__dirname + '/view/admin_dashboard.html'));
	
	
	})

	
// Router Untuk Blog ====

// Get all posts
router.get("/ambilblog", async (req, res) => {

	const ambilblog = await Blog.find()
	res.send(ambilblog)
})



// posting data

router.post("/ambilblog", async (req, res) => {
	const ambilblog = new Blog({
		judul: req.body.judul,
		isi: req.body.isi
		
	})
	await ambilblog.save()
	res.send(ambilblog)
})

// update salah satu data di database  


router.patch("/ambilblog/:id", async (req, res) => {
	try {
		const ambilblog = await Blog.findOne({ _id: req.params.id })

		if (req.body.judul) {
			ambilblog.judul = req.body.judul
		}

		if (req.body.isi) {
			ambilblog.isi = req.body.isi
		}

		if (req.body.url_gambar) {
			ambilblog.url_gambar = req.body.url_gambar
		}

		await ambilblog.save()
		res.send(ambilblog)
	} catch {
		res.status(404)
		res.send({ error: "Post doesn't exist!" })
	}
})

// delete
router.delete("/ambilblog/:id", async (req, res) => {
	try {
		await Blog.deleteOne({ _id: req.params.id })
		res.status(204).send()
	} catch {
		res.status(404)
		res.send({ error: "Post doesn't exist!" })
	}
})

// ambil satu data

router.get("/ambilblog/:id", async (req, res) => {
	try {
		const ambilblog = await Blog.findOne({ _id: req.params.id })
		res.send(ambilblog)
	} catch {
		res.status(404)
		res.send({ error: "Post doesn't exist!" })
	}
})





//========= Router Untuk Berita ====
// Get all posts
router.get("/ambil", async (req, res) => {

	const ambil = await Post.find()
	res.send(ambil)
})








// posting data

router.post("/ambil", async (req, res) => {
	const ambil = new Post({
		title: req.body.title,
		content: req.body.content,
		url_gambar: req.body.url_gambar
	})
	await ambil.save()
	res.send(ambil)
})



// update salah satu data di database  
router.patch("/ambil/:id", async (req, res) => {
	try {
		const ambil = await Post.findOne({ _id: req.params.id })

		if (req.body.title) {
			ambil.title = req.body.title
		}

		if (req.body.content) {
			ambil.content = req.body.content
		}

		if (req.body.url_gambar) {
			ambil.url_gambar = req.body.url_gambar
		}

		await ambil.save()
		res.send(ambil)
	} catch {
		res.status(404)
		res.send({ error: "Post doesn't exist!" })
	}
})





router.delete("/ambil/:id", async (req, res) => {
	try {
		await Post.deleteOne({ _id: req.params.id })
		res.status(204).send()
	} catch {
		res.status(404)
		res.send({ error: "Post doesn't exist!" })
	}
})




router.get("/ambil/:id", async (req, res) => {
	try {
		const ambil = await Post.findOne({ _id: req.params.id })
		res.send(ambil)
	} catch {
		res.status(404)
		res.send({ error: "Post doesn't exist!" })
	}
})










module.exports = router