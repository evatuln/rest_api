const express = require("express")

const mongoose = require("mongoose")
const routes = require("./routes")


mongoose
	//.	connect("mongodb://localhost:27017/rest_api_si4b", { useNewUrlParser: true })
	.connect("mongodb+srv://Nisasa:nisasa123@cluster0.ztz55.mongodb.net/rest_api_si4b?retryWrites=true&w=majority", { useNewUrlParser: true })
	.then(() => {
		const app = express()
		app.use(express.json()) // new
		app.use("/api", routes)

		app.listen(5000, () => {
			console.log("Server has started!")
		})
	})